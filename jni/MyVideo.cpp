#include <jni.h>
#include <unistd.h>

#include "com_example_myvideo_MyVideoActions.h"
#include "MyLog.h"
#include "MyCodec.h"

MyCodec ObjMyCodec;


jint Java_com_example_myvideo_MyVideoActions_naInit (JNIEnv *pEnv, jclass pObj, jstring pfilename)
{
	ObjMyCodec.init((char *)pEnv->GetStringUTFChars(pfilename, NULL));
	LOGI("native call to naInit done......");
	return 0;

}


jintArray Java_com_example_myvideo_MyVideoActions_naGetVideoRes(JNIEnv *pEnv, jclass pObj)
//jintArray naGetVideoRes(JNIEnv *pEnv, jobject pObj)
{
	jintArray lRes;
	lRes = pEnv->NewIntArray(2);
	jint lVideoRes[2];
	ObjMyCodec.getVideoRes(lVideoRes);
	pEnv->SetIntArrayRegion(lRes, 0, 2, lVideoRes);
	return lRes;
}

jint JNICALL Java_com_example_myvideo_MyVideoActions_naGetDuration(JNIEnv *pEnv, jclass pObj)
{
	return (jint)ObjMyCodec.getDuration();
}

jint JNICALL Java_com_example_myvideo_MyVideoActions_naGetFrameRate(JNIEnv *pEnv, jclass pObj)
//jint naGetFrameRate(JNIEnv *pEnv, jobject pObj)
{
	return (jint)ObjMyCodec.getFrameRate();
}

jint JNICALL Java_com_example_myvideo_MyVideoActions_naFinish(JNIEnv *pEnv, jclass pObj)
{
	LOGI("native function naFinish >>");
	ObjMyCodec.deinit();
	return 0;
}

void JNICALL Java_com_example_myvideo_MySurfaceView_native_1loop_1start(JNIEnv *, jclass)
{
	LOGI("native main start >>");
	ObjMyCodec.startLoop();
}


void JNICALL Java_com_example_myvideo_MyRenderer_native_1gl_1render(JNIEnv *, jclass)
{
	LOGI("native function native_1gl_1render >>");
	ObjMyCodec.render();
	LOGI("native function native_1gl_1render <<");
}

void JNICALL Java_com_example_myvideo_MyRenderer_native_1gl_1resize(JNIEnv *, jclass, jint width, jint height)
{
	LOGI("native function native_1gl_1resize w: %d  h:%d>>",width,height);
	ObjMyCodec.resize(width, height);
}
