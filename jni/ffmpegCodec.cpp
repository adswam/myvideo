#include <jni.h>
#include <android/bitmap.h>
#include "com_example_myvideo_MyVideoActions.h"

#include <unistd.h>
#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/mem.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>

#include <libavutil/avutil.h>
#include <libavutil/mathematics.h>
#include <libavutil/time.h>
#include <pthread.h>
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <stdint.h>

#ifdef __cplusplus
}
#endif

#include "MyLog.h"

#define RNDTO2(X) ( ( (X) & 0xFFFFFFFE )
#define RNDTO32(X) ( ( (X) % 32 ) ? ( ( (X) + 32 ) & 0xFFFFFFE0 ) : (X) )

#define TEXTURE_WIDTH  1280//960// //640//1080 1280x720
#define TEXTURE_HEIGHT 720//600 //320//720

#define S_PIXELS_SIZE (sizeof(s_pixels[0]) * TEXTURE_WIDTH * TEXTURE_HEIGHT)
static pthread_cond_t s_vsync_cond;
static pthread_mutex_t s_vsync_mutex;
static GLuint s_texture = 0;
#define MY_SCREEN_WIDTH  1280
#define MY_SCREEN_HEIGHT 720
static int s_w;
static int s_h;

static int mStartRendering = 0;

/*
 * find /opt/android-ndk-r10e/sources/ffmpeg-2.8.2/android/arm/include/ -type f -exec grep -Hn "swr_init" {} \;
 *
 */


typedef struct VideoState {
	AVFormatContext *pFormatCtx;
	AVStream *pVideoStream;
	int videoStreamIdx;
	AVFrame* frame;  //to store the decoded frame
	int fint;
	int64_t nextFrameTime;//track next frame display time
	int status;
}VideoState;
typedef struct VideoDisplayUtil {
	struct SwsContext *img_resample_ctx;
	AVFrame *pFrameRGBA;
	int width, height;
	int frameNum;
} VideoDisplayUtil;


int naPrepareDisplay(VideoState* vs,VideoDisplayUtil *&vdu, uint8_t *aBuffer) {
	LOGI("naPrepareDisplay >");
	vdu = (VideoDisplayUtil*) av_mallocz(sizeof(VideoDisplayUtil));
	vs->frame = av_frame_alloc();
	vdu->frameNum = 0;
	vdu->width = s_w;
	vdu->height = s_h;
	vdu->pFrameRGBA = av_frame_alloc();
	/* Allocate the converted picture. */
	//avpicture_alloc((AVPicture*)vdu->pFrameRGBA, AV_PIX_FMT_RGBA, s_w, s_h);
	// Determine required buffer size and allocate buffer
	int numBytes2 = avpicture_get_size(PIX_FMT_RGBA, s_w, s_h);
	aBuffer = (uint8_t *)av_malloc(numBytes2*sizeof(uint8_t));
	avpicture_fill((AVPicture *)vdu->pFrameRGBA, aBuffer,PIX_FMT_RGBA,s_w, s_h);
	LOGI("naPrepareDisplay > 4");
	vdu->img_resample_ctx = sws_getContext(vs->pVideoStream->codec->width, vs->pVideoStream->codec->height, vs->pVideoStream->codec->pix_fmt, s_w, s_h, AV_PIX_FMT_RGBA, SWS_BICUBIC, NULL, NULL, NULL);
	vs->nextFrameTime = av_gettime() + 50*1000;    //introduce 50 milliseconds of initial delay
	LOGI("naPrepareDisplay > 5");
	return 0;
}


void naInit(char *aFileName, VideoState *&vs){
	s_w = MY_SCREEN_WIDTH;
	s_h = MY_SCREEN_HEIGHT;
	LOGI("video file name is %s", aFileName);
	av_register_all();
	vs = (VideoState *) av_mallocz(sizeof(VideoState));
	av_register_all();
	//open the video file
	avformat_open_input(&vs->pFormatCtx, aFileName, NULL, NULL);
	//retrieve stream info
	avformat_find_stream_info(vs->pFormatCtx, NULL);
	//find the video stream
	int i;
	AVCodecContext *pcodecctx;
	//find the first video stream
	vs->videoStreamIdx = -1;
	for (i = 0; i < vs->pFormatCtx->nb_streams; ++i) {
		if (AVMEDIA_TYPE_VIDEO == vs->pFormatCtx->streams[i]->codec->codec_type) {
			vs->videoStreamIdx = i;
			vs->pVideoStream = vs->pFormatCtx->streams[i];
			break;
		}
	}
	//get the decoder from the video stream
	pcodecctx = vs->pFormatCtx->streams[vs->videoStreamIdx]->codec;
	AVCodec *pcodec;
	pcodec = avcodec_find_decoder(pcodecctx->codec_id);
	//open the codec
	avcodec_open2(pcodecctx, pcodec, NULL);

}


void naGetVideoRes(int *aRes, VideoState *vs){
	AVCodecContext* vCodecCtx = vs->pVideoStream->codec;
	aRes[0] = vCodecCtx->width;
	aRes[1] = vCodecCtx->height;
}

int naGetDuration(VideoState *vs){
	return (vs->pFormatCtx->duration / AV_TIME_BASE);
}

int naGetFrameRate(VideoState *vs){
	int fr;
	if(vs->pVideoStream->avg_frame_rate.den && vs->pVideoStream->avg_frame_rate.num) {
		fr = av_q2d(vs->pVideoStream->avg_frame_rate);
	} else if(vs->pVideoStream->r_frame_rate.den && vs->pVideoStream->r_frame_rate.num) {
		fr = av_q2d(vs->pVideoStream->r_frame_rate);
	} else if(vs->pVideoStream->time_base.den && vs->pVideoStream->time_base.num) {
		fr = 1/av_q2d(vs->pVideoStream->time_base);
	} else if(vs->pVideoStream->codec->time_base.den && vs->pVideoStream->codec->time_base.num) {
		fr = 1/av_q2d(vs->pVideoStream->codec->time_base);
	}
	return fr;
}


int naFinish(VideoState* vs, VideoDisplayUtil* vdu, uint8_t *aBuffer){
	LOGI("native function naFinish >>");
	//close codec
	if(vs != NULL) {
		if (vs->pVideoStream != NULL){
			avcodec_close(vs->pVideoStream->codec);
		}
		LOGI("native function naFinish >> codec closed");
		//close video file
		avformat_close_input(&vs->pFormatCtx);
		LOGI("native function naFinish >> close input");
		av_free(aBuffer);
		av_free(vdu);
		av_free(vs);
	}
	return 0;
}

void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame, char *aVideoFileName)
{
	FILE *pFile;
	char szFilename[820];
	int  y;
	LOGI("save frame >>");
	// Open file
	char *folderName = aVideoFileName;
	LOGI("full folder name:%s", folderName);
	while (folderName[0] !=  '4'){
		folderName++;
		//LOGI(" + copy name:%c", *folderName);
	}
	while(folderName[0] !=  '/'){
		folderName--;
		//LOGI(" - copy name:%c", *folderName);
	}
	folderName++;
	LOGI("folder name:%s", folderName);
	int ind = 0;
	while((aVideoFileName + ind) < folderName){
		szFilename[ind] = aVideoFileName[ind];
		//LOGI("copy name:%c", gVideoFileName[ind]);
		ind++;
	}
	szFilename[ind++] = 'f';szFilename[ind++] = '.';szFilename[ind++] = 'p';szFilename[ind++] = 'p';szFilename[ind++] = 'm';
	szFilename[ind++] = '\0';
	LOGI("save frame in:%s", szFilename);
	//sprintf(szFilename, "frame.ppm", iFrame);
	pFile=fopen(szFilename, "wb");
	if(pFile==NULL)
		return;

	// Write header
	fprintf(pFile, "P6\n%d %d\n255\n", width, height);

	// Write pixel data
	for(y=0; y<height; y++)
		fwrite(pFrame->data[0]+y*pFrame->linesize[0], 1, width*3,  pFile);
	// Close file
	fclose(pFile);
}

int naGetVideoFrame(VideoState* vs,VideoDisplayUtil* vdu, char *mVideoFileName) {
	//read frames and decode them
	AVPacket packet;
	int framefinished;
	LOGI("naGetVideoFrame >>");
	while ((!vs->status) && 0 <= av_read_frame(vs->pFormatCtx,&packet)) {
		if (vs->videoStreamIdx == packet.stream_index) {
			avcodec_decode_video2(vs->pVideoStream->codec, vs->frame,&framefinished, &packet);
			if (framefinished) {
				sws_scale(vdu->img_resample_ctx, vs->frame->data,vs->frame->linesize, 0, vs->pVideoStream->codec->height, vdu->pFrameRGBA->data, vdu->pFrameRGBA->linesize);
				int64_t curtime = av_gettime();
				if(vdu->frameNum == 0){
					SaveFrame(vdu->pFrameRGBA, s_w, s_h, vdu->frameNum, mVideoFileName);
					// SaveFrame(vs->frame, gvs->pVideoStream->codec->width, gvs->pVideoStream->codec->height, vdu->frameNum);
				}
				if (vs->nextFrameTime - curtime > 20*1000) {
					usleep(vs->nextFrameTime-curtime);
				}
				++vdu->frameNum;
				vs->nextFrameTime += vs->fint*1000;
				LOGI("naGetVideoFrame %d <<", vdu->frameNum);
				LOGI("naGetVideoFrame <<");
				return vdu->frameNum;
			}
		}
		av_free_packet(&packet);
	}
	LOGI("naGetVideoFrame outside loop<<")
	return 0;
}

static void check_gl_error(const char* op)
{
	GLint error;
	for (error = glGetError(); error; error = glGetError())
		LOGI("after %s() glError (0x%x)\n", op, error);
}
/* wait for the screen to redraw */
static void wait_vsync()
{
	pthread_mutex_lock(&s_vsync_mutex);
	pthread_cond_wait(&s_vsync_cond, &s_vsync_mutex);
	pthread_mutex_unlock(&s_vsync_mutex);
}



int naRender(VideoDisplayUtil* vdu){
	if (mStartRendering == 1){
		LOGI("render >>");
		glClear(GL_COLOR_BUFFER_BIT);
		//LOGI("render starting >>");
		glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
		//SaveFrame(vdu->pFrameRGBA, s_w, s_h, vdu->frameNum);
		glTexImage2D(GL_TEXTURE_2D,		        /* target */
				0,			                    /* level */
				GL_RGBA,			            /* internal format */
				TEXTURE_WIDTH,		            /* width */
				TEXTURE_HEIGHT,		            /* height */
				0,			                    /* border */
				GL_RGBA ,			            /* format */
				GL_UNSIGNED_BYTE,               /* type  GL_UNSIGNED_BYTE GL_UNSIGNED_SHORT_4_4_4_4*/
				(const GLvoid *)vdu->pFrameRGBA->data[0] );		/* pixels s_pixels vs->frame->data[0] vdu->pFrameRGBA->data[0]*/

		check_gl_error("glTexImage2D");
		LOGI("render image render to screen");
		glDrawTexiOES(0, 0, 0, TEXTURE_WIDTH, TEXTURE_HEIGHT);
		check_gl_error("glDrawTexiOES");

		/* tell the other thread to carry on */
		pthread_cond_signal(&s_vsync_cond);
		LOGI("render  <<");
	}
	return 0;
}

void loop_start(VideoState* vs,VideoDisplayUtil* vdu,char *aVideoFileName) {
	LOGI("native main start >>");
	pthread_cond_init(&s_vsync_cond, NULL);
	pthread_mutex_init(&s_vsync_mutex, NULL);
	while (true) {
		if (0 < naGetVideoFrame(vs, vdu,aVideoFileName)) {
			LOGI("native main loop got a frame..");
			mStartRendering = 1;
			//wait on screen sync
			wait_vsync();
		}else{
			break;     //failure, break
		}
	}
}

void naResize(int width, int height) {
	LOGI("native function native_1gl_1resize w: %d  h:%d>>",width,height);
	/* store the actual width of the screen */
	//s_w = width;
	//s_h = height;
	glDeleteTextures(1, &s_texture);
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &s_texture);
	glBindTexture(GL_TEXTURE_2D, s_texture);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glColor4x(0x10000, 0x10000, 0x10000, 0x10000);
	check_gl_error("glColor4x");
	int rect[4] = {0, TEXTURE_HEIGHT, TEXTURE_WIDTH, -TEXTURE_HEIGHT};
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_CROP_RECT_OES, rect);
	check_gl_error("glTexParameteriv");
}
