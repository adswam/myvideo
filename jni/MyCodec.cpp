#include <unistd.h>

#include "MyLog.h"
#include "MyCodec.h"


void naInit(char *aFileName, VideoState *&vs);
int naPrepareDisplay(VideoState* vs,VideoDisplayUtil *&vdu, uint8_t *aBuffer);
void naGetVideoRes(int *aRes, VideoState *vs);
int naGetDuration(VideoState *vs);
int naGetFrameRate(VideoState* vs);
void naFinish(VideoState* vs,VideoDisplayUtil* vdu, uint8_t *aBuffer);
void loop_start(VideoState* vs,VideoDisplayUtil* vdu, char *mVideoFileName);
int naRender(VideoDisplayUtil* vdu);
void naResize(int width, int height);

MyCodec::MyCodec(){
	LOGI("Creating MyCodec....");
	mVideoState = 0;
	mVideoDisplayUtil = 0;
	mVideoFileName = 0;
	mBuffer= 0;
}

void MyCodec::init(char *aFileName){
	mVideoFileName = aFileName;
	naInit(mVideoFileName, mVideoState);
	LOGI("init done ....");
	naPrepareDisplay(mVideoState, mVideoDisplayUtil, mBuffer);
	LOGI("prepare display done.....");
}

void MyCodec::getVideoRes(int *aRes){
	naGetVideoRes(aRes, mVideoState);
}

int MyCodec::getDuration(){
	return naGetDuration(mVideoState);
}

int MyCodec::getFrameRate(){
	return naGetFrameRate(mVideoState);
}

void MyCodec::deinit(){
	naFinish(mVideoState, mVideoDisplayUtil, mBuffer);
}

void MyCodec::startLoop(){
	loop_start(mVideoState, mVideoDisplayUtil,mVideoFileName);
}

void MyCodec::render(){
	naRender(mVideoDisplayUtil);
}

void MyCodec::resize(int aWidth, int aHeight){
	naResize(aWidth, aHeight);
}
