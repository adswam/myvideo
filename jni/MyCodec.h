#ifndef MY_CODEC
#define MY_CODEC

struct VideoState;
struct VideoDisplayUtil;

class MyCodec{
public:
	MyCodec();
	void init(char *aFileName);
	void getVideoRes(int *aRes);
	int getDuration();
	int getFrameRate();
	void startLoop();
	void render();
	void resize(int aWidth, int aHeight);
	void deinit();
private:
	char *mVideoFileName;
	VideoState *mVideoState ;
	VideoDisplayUtil *mVideoDisplayUtil;
	uint8_t *mBuffer;
};

#endif
