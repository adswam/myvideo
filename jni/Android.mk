LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := myvideo
LOCAL_SRC_FILES := ffmpegCodec.cpp MyCodec.cpp MyVideo.cpp
LOCAL_LDLIBS := -llog -lEGL -lGLESv1_CM -lz -L$(LOCAL_PATH)/ffmpeg282/android/arm/lib -lavformat -lavcodec -lavutil -lswscale -lswresample
LOCAL_STATIC_LIBRARIES := libavformat libavcodec libavutil 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/ffmpeg282/android/arm/include
LOCAL_CFLAGS += -D GL_GLEXT_PROTOTYPES
include $(BUILD_SHARED_LIBRARY)
