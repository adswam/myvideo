package com.example.myvideo;
import java.io.File;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.util.Log;

public class MyRenderer implements GLSurfaceView.Renderer{
	private File mmediaFile;
	private String mStrMediaFile;	
	private int mTextureID;
	private SurfaceTexture mSurface;
	private int GL_TEXTURE_EXTERNAL_OES = 0x8D65;
	private MyVideoActions mMVA;
	private static native void native_gl_render(String MediaFile);
	private static native void native_gl_resize(int width, int height);	
	static {
	       System.loadLibrary("myvideo");
	}
	public void initFile(File aFile, MyVideoActions MVA){
		mmediaFile = aFile;
		mStrMediaFile = new String(mmediaFile.toString());
		if (MVA != null){
			mMVA = MVA;
		}else{
			Log.d("MyRenderer","Error video actions object empty");
		}

		Log.d("MyRenderer str",mStrMediaFile);
	}
	@Override
	public void onDrawFrame(GL10 gl) {
		// TODO Auto-generated method stub
		native_gl_render(mStrMediaFile);
		
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// TODO Auto-generated method stub
		native_gl_resize(width, height);		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// TODO Auto-generated method stub	
	}
}
