package com.example.myvideo;

import java.io.File;

import android.util.AttributeSet;






public class MyVideoAction {
	private static native int naInit(String pVideoFileName);
	private static native int[] naGetVideoRes();
	private static native int naFinish();
	private static native int naGetDuration();
	private static native int naGetFrameRate();
	private static native void native_gl_render(String MediaFile);
	private static native void native_gl_resize(int width, int height);	
	private static native void native_loop_start(AttributeSet attributeset);
	static {
	       System.loadLibrary("myvideo");
	}
	
	public String mp4File;


}	
