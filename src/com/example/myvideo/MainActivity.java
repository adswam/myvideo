package com.example.myvideo;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;

import android.app.Activity;
import android.content.res.Resources;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;
import android.view.View.OnTouchListener;

public class MainActivity extends Activity {

	private Button buttonStart;
	private Button buttonStartVideo;
	private Button buttonStop;
	private Button buttonPlay;
	private Spinner spinnerFileList;
	private GLSurfaceView mGLView;
	private MyVideoActions iMva;
	private MySurfaceView objMySurfaceView;
	private File mMediaFile;
	private String mPath;
	private ArrayAdapter<String> dataAdapter;
	private final String TAG = "MyVideo";
	private List<File> list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	}
	
	@Override
	public void onResume() {
	    super.onResume();  // Always call the superclass method first

	    buttonStart = (Button) findViewById(R.id.button1);
		buttonStartVideo = (Button) findViewById(R.id.button2);
		spinnerFileList = (Spinner) findViewById(R.id.spinner1);
		
		spinnerFileList.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				 String item = parent.getItemAtPosition(position).toString();
			      
			      // Showing selected spinner item
			     Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
		    	 Log.d("MyVideo", "Item selected :" + item + "  list size: " + list.size() + " > postion : " +position);
		    	 mMediaFile = list.get(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
	     });
		      
		buttonStart.setOnClickListener(new OnClickListener() {
		       @Override
		       public void onClick(View v) {	
		    	   Log.d("MyVideo", "Intialialse Button clicked");
		    	   EditText txtFolder = (EditText) findViewById(R.id.editText1);
		    	   mPath = txtFolder.getText().toString();
		    	   Log.d("MyVideo", mPath);
		    	   File f = null;
				   if( setFile(mPath , f)){
			    	   startMyVideo();					   
				   } else {
			    	   Log.d("MyVideo", "ERROR: file path has not been set");					   
				   }

		       }
		     });
		buttonStartVideo.setOnClickListener(new OnClickListener() {
		       @Override
		       public void onClick(View v) {
			     // TODO Auto-generated method stub
		    	   Log.d("MyVideo", "StartVideo Button clicked");   		    		   	   		   
		   		   Resources res = MainActivity.this.getResources();                       
		   		   XmlPullParser parser = res.getXml(R.layout.activity_video);   
		   		   AttributeSet attributes = Xml.asAttributeSet(parser);  
		   		   //objMySurfaceView = new MySurfaceView(getApplicationContext(), attributes );
		    	   Log.d("MyVideo", "How many times native loop called?"); 
		   		   setContentView(R.layout.activity_video);
		   		   
		   		   buttonPlay = (Button) findViewById(R.id.play_button);
		   		   buttonStop = (Button) findViewById(R.id.stop_button);
		   		   
		   		   buttonStop.setOnClickListener(new OnClickListener() {
		   			   @Override
		   			   public void onClick(View v) {
		   				   // TODO Auto-generated method stub
		   				   Log.d("MyVideo", "stop Button clicked");
		   				   if(iMva != null){
		   					   iMva.MyVideoActionFinish();
		   				   }
	   			   		   setContentView(R.layout.activity_main);
				       }
					});
		   		   buttonPlay.setOnClickListener(new OnClickListener() {
				       @Override
				       public void onClick(View v) {
					     // TODO Auto-generated method stub
				    	   Log.d("MyVideo", "play Button clicked");	  
		   				   if(iMva != null){
		   					   iMva.MyVideoActionPlay();
		   				   }
				       }
					});
		       }
			});
		
	}
		
	private boolean setFile(String txtFolder, File spinItem ) {
		Log.d(TAG, "MyVideo getFile : folder :" + txtFolder + "spinnerItem :" + spinItem);
		boolean isFileSet = false;
		try {	
			String path = Environment.getExternalStorageDirectory().getAbsolutePath().toString()+ txtFolder;
			Log.d(TAG, "Path: " + path);
			File f = new File(path);  
			if (f.isDirectory())
				Log.d("MyVideo", "found video Directory(samples) in sdcard ");
			File file[] = f.listFiles();
			if (spinItem != null){
				mMediaFile = spinItem ;
				isFileSet = true;
			} else if (file != null) {
				Log.d("MyVideo", "Size: "+ file.length);
				list = new Vector<File>();
				for (int i=0; i < file.length; i++) {
					//Log.d("MyVideo", "FileName:" + file[i].getName());
					if (file[i].getName().contains(".mp4")) {
							list.add(file[i]);
							//Log.d(TAG, "MyVideo getFile : " + file[i].getName().toString()+ " vec size:" + list.size());
							mMediaFile = file[i];
							isFileSet = true;
						    Log.d("MyVideo", " mMediaFile:" + mMediaFile.toString());
					}
				}
				
					
				//	seems below log line is needed for display else list is empty in display
					Log.d(TAG, " vec size:" + list.size());	
					ArrayList<String> strList = new ArrayList<String>();
					for(File element: list){
						strList.add(element.getName().toString());
						Log.d(TAG, "MyVideo getFile : " + element.getName().toString());	
					}
					Log.d(TAG, " strlist size:" + strList.size());
					dataAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, strList);
					dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	                spinnerFileList.setAdapter(dataAdapter); 
					
			 
			} else {
				Log.d("MyVideo", "Video File not found in sdcardFile!!!!!!!!!!" );
			}
		} catch (Throwable t) {
			 	Log.e("MyVideo", "Caught error in threadmyVideo : %s" + t.toString());
		}
		return isFileSet;
	}
	public void startMyVideo(){
		Thread threadMyVideo = new Thread(new Runnable() {
			public void run() {
				try {	
					iMva = new MyVideoActions(mMediaFile);		
				}
				catch (Throwable t) {
				 	Log.e("MyVideo", "Caught error in threadmyVideo : %s" + t.toString());
				}
			}		
		});
		threadMyVideo.start();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}



}
