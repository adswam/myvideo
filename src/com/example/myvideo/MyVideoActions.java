package com.example.myvideo;

import java.io.File;

import android.os.Environment;
import android.util.Log;



public class MyVideoActions {
	private static native int naInit(String pVideoFileName);
	private static native int[] naGetVideoRes();
	private static native int naFinish();
	private static native int naGetDuration();
	private static native int naGetFrameRate();


	static {
	       System.loadLibrary("myvideo");
	}
	
	public String mp4File;
	public MyVideoActions(File aFile) {
		Log.d("MyVideoActions",aFile.toString());
        Log.d("Files", "FileName:" + aFile.getName());
		if (aFile.getName().contains(".mp4"))
		{
			naInit(aFile.toString());
			int iDur = naGetDuration();
			Log.d("Files", " mp4 FileName:" + mp4File + "  ...Duration : " + Integer.toString(iDur));
		}
	}
	public void MyVideoActionFinish(){
		Log.d("MyVideoActions","MyVideoActionFinish()");
		naFinish();
	}
	public void MyVideoActionPlay(){
		Log.d("MyVideoActions","MyVideoActionPlay()");

	}

}	
