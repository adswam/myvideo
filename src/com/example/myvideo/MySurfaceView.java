package com.example.myvideo;
import java.io.File;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

public class MySurfaceView extends GLSurfaceView{
	private MyRenderer mRenderer;
	private static native void native_loop_start();

	static {
	       System.loadLibrary("myvideo");
	}
	public MySurfaceView(Context context, AttributeSet attributeset) {
		super(context, attributeset);
		// TODO Auto-generated constructor stub
		(new Thread() {
            @Override
            public void run() {
		    	   Log.d("MySurfaceView", "Start native loop");   
                   native_loop_start();
            }
        }).start();
		mRenderer = new MyRenderer();
	    this.setRenderer(mRenderer);
	   // this.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}


}
